﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Windows.Forms;

namespace TxJsq
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private static Point movePoint;
        private void Form1_Shown(object sender, EventArgs e)
        {
            int x = this.Location.X + 290;
            int y = this.Location.Y;
            movePoint = new Point(x, y);
            if (File.Exists(@"D:\TxJsq\" + exe + ".exe"))
                setTx("D");
            else if (File.Exists(@"E:\TxJsq\" + exe + ".exe"))
                setTx("E");
            else if (File.Exists(@"F:\TxJsq\" + exe + ".exe"))
                setTx("F");
            else if (File.Exists(@"G:\TxJsq\" + exe + ".exe"))
                setTx("G");
            else
                setTx("D");
        }

        private void msg(string msg)
        {
            MessageBox.Show(this, msg, "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private string tx = @"D:\";
        private string jsq = @"TxJsq\";
        private string exe = "QMProxyAcceler";

        private void setTx(string pan) {
            tx = pan + @":\";
            this.textBox1.Text = tx + jsq;
        }
        private string getJxq()
        {
            return tx + jsq + exe + ".exe";
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            string pan = this.textBox1.Text.ToUpper().Substring(0, 1);
            if ("DEFG".Contains(pan))
                setTx(pan);
            else
                setTx("D");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tx))
            {
                msg("目录选择错误！");
                return;
            }
            if (!File.Exists(getJxq()))
            {
                String pack = @"pack.zip";
                this.button1.Enabled = false;
                File.WriteAllBytes(pack, Properties.Resources.TxJsq);
                ZipFile.ExtractToDirectory(pack, tx);
                File.Delete(pack);
                this.button1.Enabled = true;
            }
            msg("已解压到文件夹 " + tx + jsq + " ！");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Process p = new Process();
            p.StartInfo.FileName = "cmd.exe";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.CreateNoWindow = true;
            p.Start();
            p.StandardInput.AutoFlush = true;
            p.StandardInput.WriteLine("netsh advfirewall firewall del rule dir=out name=disable_TxJsq_update");
            p.StandardInput.WriteLine("netsh advfirewall firewall add rule dir=out name=disable_TxJsq_update action=block program=" + tx + jsq + @"update\" + exe + ".exe");
            p.StandardInput.WriteLine("netsh advfirewall firewall del rule dir=out name=disable_TxJsq_update2");
            p.StandardInput.WriteLine("netsh advfirewall firewall add rule dir=out name=disable_TxJsq_update2 action=block program=" + tx + jsq + @"update\QMProxyAccUpdate.exe");
            p.StandardInput.WriteLine("exit");
            msg(p.StandardOutput.ReadToEnd().ToString());
            p.WaitForExit();
            p.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!File.Exists(getJxq()))
            {
                msg("没找到文件，请先执行第一步解压！");
                return;
            }
            Process[] ps = Process.GetProcessesByName(exe);
            if (ps.Length == 0)
            {
                Process.Start(getJxq(), "/entry=InstallDir");
            }
            this.Location = movePoint;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Process.Start("https://gitee.com/liuzy1988/TxJsq");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Process[] ps = Process.GetProcessesByName(exe);
            if (ps.Length == 0)
            {
                msg("没有找到" + exe + "进程！");
                return;
            }
            Process p = ps[0];

            IntPtr processHandle = NativeMethods.OpenProcess(2035711u, false, p.Id);
            if (processHandle == IntPtr.Zero)
            {
                msg("打开进程" + exe + "失败！");
                return;
            }

            IntPtr baseAddress = p.MainModule.BaseAddress;

            WriteInt(processHandle, baseAddress, 1139712, 1);
            WriteInt(processHandle, baseAddress, 1151216, 2);
            WriteInt(processHandle, baseAddress, 1152304, 0);
            WriteInt(processHandle, baseAddress, 1152312, 3);
            WriteInt(processHandle, baseAddress, 1152316, GetLongTime());
            WriteInt(processHandle, baseAddress, 1153660, 1);

            NativeMethods.CloseHandle(processHandle);
            msg("已破解，请点击头像查看到期时间！");
        }

        static long GetLongTime()
        {
            DateTime current = new DateTime(2099, 12, 12, 12, 12, 12);
            DateTime dt0 = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return (current.Ticks - dt0.Ticks) / 10000 / 1000;
        }

        static void WriteInt(IntPtr processHandle, IntPtr baseAddress, int offset, long value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            NativeMethods.WriteProcessMemory(processHandle, IntPtr.Add(baseAddress, offset), bytes, new IntPtr(bytes.Length), IntPtr.Zero);
        }
    }
}
